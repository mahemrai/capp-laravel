<?php
namespace Utils;

class ResponseUtil{
    /**
     * Confirm whether the request was successful or not.
     * @param string $response
     * @return boolean
     */
    public static function isSuccessful($json_response){
        $response = json_decode($json_response);

        if($response->response->status->code == 0){
            return true;
        }
        else return false;
    }
}
?>