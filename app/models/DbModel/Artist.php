<?php
namespace DbModel;

use Illuminate\Database\Eloquent\Model;

/**
 * Artist model class
 * @author mahendra
 *
 */

class Artist extends Model{
    protected $table = 'artists';
    protected $primaryKey = 'mbid';

    public static $factory = array(
        'name' => 'string',
        'mbid' => 'string',
        'image' => 'string'
    );

    //constructor
    public function __construct(){
        parent::__construct();
    }

    /**
     * Defines relationship with album table.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums(){
        return $this->hasMany('DbModel\Album', 'artist_id', 'mbid');
    }

    /**
     * Creates new artist.
     * @param string $name
     * @param string $mbid
     * @param string $image
     * @return boolean
     */
    public function createArtist($name, $mbid, $image){
        $this->name = $name;
        $this->mbid = $mbid;
        $this->image = $image;

        return ($this->save()) ? true : false;
    }

    /**
     * Checks if the artist already exists in the database.
     * @param string $name
     * @return Artist
     */
    public function fetchArtist($key, $value){
        if(strcasecmp($key, 'name') == 0){
            return self::where(
                    strtolower('name'),
                    strtolower($value)
            )->get()->toArray();
        }
        elseif(strcasecmp($key, 'mbid') == 0){
            return self::where(
                'mbid', $value
            )->get()->toArray();
        }
    }

    /**
     * Gets list of artists.
     * @return array
     */
    public function fetchArtists($is_recent=false){
        if($is_recent){
            return self::all(array('name','mbid','image'))
                ->reverse()
                ->take(5)
                ->toArray();
        }
        else{
            return self::paginate(10);
        }
    }

    /**
     * Gets artist info including related albums.
     * @param unknown $artist_id
     */
    public function fetchArtistInfo($artist_id){
        return self::find($artist_id)
            ->where('mbid', $artist_id)
            ->with('albums')
            ->get()
            ->toArray();
    }
}