<?php
namespace DbModel;

use Illuminate\Database\Eloquent\Model;

class Track extends Model{
    protected $table = 'tracks';

    public static $factory = array(
        'album_id' => 'factory|DbModel\Track',
        'name' => 'string',
        'duration' => 'integer'
    );

    //constructor
    public function __construct(){
        parent::__construct();
    }

    public function album(){
        return $this->belongsTo('DbModel\Album', 'mbid', 'album_id');
    }

    /**
     * Creates new track.
     * @param string $album_id
     * @param array $tracks
     */
    public function createTrack($album_id, $tracks){
        foreach($tracks['track'] as $track){
            $row = new self();
            $row->album_id = $album_id;
            $row->name = $track['name'];
            $row->duration = $track['duration'];

            if(!$row->save()) return false;
        }

        return true;
    }

    public function fetchTracks($album_id){
        return self::where('album_id', $album_id)->get()->toArray();
    }
}
?>