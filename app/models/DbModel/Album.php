<?php
namespace DbModel;

use Illuminate\Database\Eloquent\Model;

/**
 * Album model class
 * @author mahendra
 */

class Album extends Model{
    protected $table = 'albums';
    protected $primaryKey = 'mbid';

    public static $factory = array(
        'artist_id' => 'factory|DbModel\Artist',
        'name' => 'string',
        'mbid' => 'string',
        'image' => 'string'
    );

    //constructor
    public function __construct(){
        parent::__construct();
    }

    /**
     * Defines relation with artist table.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function artist(){
        return $this->belongsTo('DbModel\Artist', 'artist_id', 'mbid');
    }

    /**
     * Defines relation with tracks table.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tracks(){
        return $this->hasMany('DbModel\Track', 'album_id', 'mbid');
    }

    /**
     * Creates new album.
     * @param string $artist_id
     * @param string $name
     * @param string $mbid
     * @param string $image
     * @return boolean
     */
    public function createAlbum($artist_id, $name, $mbid, $image){
        $this->artist_id = $artist_id;
        $this->name = $name;
        $this->mbid = $mbid;
        $this->image = $image;

        return ($this->save()) ? true : false;
    }

    /**
     * Gets selected album including related artist and tracks.
     * @param string $album_id
     */
    public function fetchAlbum($album_id){
        return self::find($album_id)->where('mbid', $album_id)
                ->with(array('artist', 'tracks'))
                ->get()
                ->toArray();
    }

    /**
     * Gets list of albums for selected artist or list of albums.
     * @param string $artist_id
     * @return array
     */
    public function fetchAlbums($artist_id=false){
        $result = null;

        if($artist_id){
            $result = self::where(
                    'artist_id', $artist_id
            )->get()->toArray();
        }
        else{
            $cols = array('name', 'mbid', 'image');
            $result = self::all($cols)
                    ->reverse()
                    ->take(5)
                    ->toArray();
        }

        return $result;
    }

    /**
     *
     * @return Ambigous <\Illuminate\Pagination\Paginator, \Illuminate\Pagination\Paginator>
     */
    public function fetchAllAlbums(){
        return self::with('artist')->paginate(10);
    }

    /**
     *
     * @param unknown $artist_id
     */
    public function fetchOtherAlbums($artist_id){
        return self::where('artist_id', $artist_id);
    }

    /**
     * Checks if the album already exists.
     * @param string $name
     */
    public function albumExists($name){
        return self::where(
            strtolower('name'), strtolower($name)
        )->get()->toArray();
    }
}