<?php
namespace ApiClient;

/**
 * Apiclient model class
 * @author mahendrarai
 */

class Apiclient{
    protected $api_url;

    //constructor
    public function __construct(){}

    /**
     * Sends HTTP request using curl.
     * @param string $url
     */
    protected function sendRequest($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_URL, $url);

        $return = curl_exec($curl);
        curl_close($curl);

        return $return;
    }
}