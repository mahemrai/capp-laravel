<?php
namespace ApiClient;

use Utils\ResponseUtil;
/**
 * Echonest model class
 * @author mahendra
 */

class Echonest extends Apiclient{
    const API_KEY = 'AHAVGTJRT7B3O76IY';

    //constructor
    public function __construct(){
        parent::__construct();
        $this->api_url = 'http://developer.echonest.com/api/v4/';
    }

    /**
     * Gets artist related news from echonest.
     * @param unknown $artist_id
     * @return Ambigous <\ApiClient\mixed, NULL, mixed>
     */
    public function getArtistNews($artist_id){
        $url = $this->api_url.'artist/news?api_key='.self::API_KEY.
               '&id=musicbrainz:artist:'.$artist_id;

        return $this->_result($url);
    }

    /**
     * Gets artists similar to selected artist from echonest.
     * @param unknown $artist_id
     * @return Ambigous <\ApiClient\mixed, NULL, mixed>
     */
    public function getSimilarArtists($artist_id){
        $url = $this->api_url.'artist/similar?api_key='.self::API_KEY.
               '&id=musicbrainz:artist:'.$artist_id.'&bucket=id:musicbrainz';

        return $this->_result($url);
    }

    /**
     * Sets result depending on the response received from echonest.
     * @param string $url
     * @return mixed|NULL
     */
    private function _result($url){
        $json_response = $this->sendRequest($url);

        if(ResponseUtil::isSuccessful($json_response)){
            return json_decode($json_response, true);
        }
        else return null;
    }
}