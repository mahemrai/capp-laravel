<?php
namespace ApiClient;

/**
 * Lastfm model class
 * @author mahendrarai
 */

class Lastfm extends Apiclient{
    const API_KEY = '2dc8fe6d2ff0548b989539ff8f03c8be';

    //constructor
    public function __construct(){
        parent::__construct();
        $this->api_url = 'ws.audioscrobbler.com/2.0/';
    }

    /**
     * Search for artist using Lastfm api.
     * @param string $artist
     * @return json string
     */
    public function artistSearch($artist){
        $url = $this->api_url.'?method=artist.search&artist='.$artist.
               '&api_key='.self::API_KEY.'&format=json';

        return $this->sendRequest($url);
    }

    /**
     * Search for albums of the selected artist using Lastfm api.
     * @param string $mbid
     * @return json string
     */
    public function albumSearch($mbid){
        $url = $this->api_url.'?method=artist.gettopalbums&mbid='.$mbid.
               '&api_key='.self::API_KEY.'&format=json';

        return $this->sendRequest($url);
    }

    /**
     * Get information for the selected artist.
     * @param string $mbid
     * @return json string
     */
    public function fetchArtistInfo($mbid){
        $url = $this->api_url.'?method=artist.getinfo&mbid='.$mbid.
               '&api_key='.self::API_KEY.'&format=json';

        return $this->sendRequest($url);
    }

    /**
     * Get information for the selected album.
     * @param string $mbid
     * @return json string
     */
    public function fetchAlbumInfo($mbid){
        $url = $this->api_url.'?method=album.getinfo&api_key='.self::API_KEY.
               '&mbid='.$mbid.'&format=json';

        return $this->sendRequest($url);
    }
}