<?php
namespace ApiClient;

/**
 * Youtube model class
 * @author mahendra
 */

class Youtube{
    const DEVELOPER_KEY = 'AIzaSyAGtRA1esIKKOGjERpS4X7BvPsLHoFL7B4';
    const MAX_RESULTS = 1;

    private $google_client;

    //constructor
    public function __construct(){
        $this->google_client = new \Google_Client();
        $this->google_client->setDeveloperKey(self::DEVELOPER_KEY);
    }

    /**
     *
     * @param string $query_string
     * @return Ambigous <Google_SearchListResponse, Google_HttpRequest, multitype:, unknown, mixed, NULL>
     */
    public function fetchVideo($query_string){
        $youtube = new \Google_YouTubeService($this->google_client);

        return $youtube->search->listSearch('id', array(
                'q' => $query_string,
                'maxResults' => self::MAX_RESULTS
        ));
    }
}