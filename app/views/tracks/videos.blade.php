@section('content')

    <div id="#videoModal" class="row">
        <div class="small-10 small-centered columns">
            <iframe id="ytplayer" type="text/html" width="640" height="390"
              src="http://www.youtube.com/embed/{{ $video[0]['id']['videoId'] }}?autoplay=1" frameborder="0" />
        </div>
    </div>

@stop