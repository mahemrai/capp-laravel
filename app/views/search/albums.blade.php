@section('content')

    <div class="row" style="padding-top:10px;">
        <div class="small-10 small-centered columns">
            <a class="button small" href="/">Back to start</a>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <table width="100%">
                <thead>
                    <th>Album</th>
                    <th>Actions</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($albums['topalbums']['album'] as $album)
                        <tr>
                            <td>{{ $album['name'] }}</td>
                            <td>
                                {{ Form::open(array('url' => '/album/add', 'method' => 'POST')) }}
                                {{ Form::hidden('album', $album['name']) }}
                                {{ Form::hidden('mbid', $album['mbid']) }}
                                {{ Form::hidden('image', $album['image'][2]['#text']) }}
                                {{ Form::hidden('artist_id', $artist_id) }}
                                {{ Form::submit('Add to my list', array('class' => 'button tiny')) }}
                                {{ Form::close() }}
                            </td>
                            <td>
                                {{ Form::open(array('url' => '/search/'.$album['artist']['name'].'/'.$album['name'], 'method' => 'POST')) }}
                                {{ Form::hidden('mbid', $album['mbid']) }}
                                {{ Form::submit('View album info', array('class' => 'button tiny')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop