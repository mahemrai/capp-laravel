@section('content')

    <div class="row" style="padding-top:10px;">
        <div class="small-10 small-centered columns">
            <a class="button small" href="/">Back to start</a>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <table width="100%">
                <thead>
                    <th>Artist</th>
                    <th>Actions</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($artists['results']['artistmatches']['artist'] as $artist)
                    <tr>
                        <td>{{ $artist['name'] }}</td>
                        <td>
                            {{ Form::open(array('url' => '/artist/add', 'method' => 'POST')) }}
                            {{ Form::hidden('artist', $artist['name']) }}
                            {{ Form::hidden('mbid', $artist['mbid']) }}
                            {{ Form::hidden('image', $artist['image'][2]['#text']) }}
                            {{ Form::submit('Add to my list', array('class' => 'button tiny')) }}
                            {{ Form::close() }}
                        </td>
                        <td>
                            {{ Form::open(array('url' => '/search/'.$artist['name'].'/albums', 'method' => 'POST')) }}
                            {{ Form::hidden('mbid', $artist['mbid']) }}
                            <button class='button tiny' style=''><i class="fa fa-search"></i> View albums</button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop