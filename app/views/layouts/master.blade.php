<!DOCTYPE html>
<html>
    <head>
        <title>Music List</title>
        <link rel="stylesheet" href="/assets/foundation/css/normalize.css">
        <link rel="stylesheet" href="/assets/foundation/css/foundation.css">
        <link rel="stylesheet" href="/assets/css/site.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="/assets/foundation/js/modernizr.js"></script>
    </head>
    <body>
        <div class="container">
            @include('layouts.navigation')
            @yield('content')
        </div>

        <script src="/assets/foundation/js/vendor/jquery.js"></script>
        <script src="/assets/foundation/js/vendor/fastclick.js"></script>
        <script src="/assets/foundation/js/foundation.min.js"></script>

        <script>
            $(document).foundation();
        </script>
    </body>
</html>