<div class="row">
    <div class="small-10 small-centered columns">
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <h1><a href="/">Music List</a></h1>
                </li>
            </ul>

            <section class="top-bar-section">
                <ul class="right">
                    <li>
                        <a href="/">Main</a>
                    </li>
                    <li>
                        <a href="/artists">Artists</a>
                    </li>
                    <li>
                        <a href="/albums">Albums</a>
                    </li>
                </ul>
            </section>
        </nav>
    </div>
</div>