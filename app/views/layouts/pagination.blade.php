@if($paginator->getLastPage() > 1)
    <ul class="pagination">
        <li class="{{ ($paginator->getCurrentPage() == 1) ? 'arrow unavailable' : '' }}">
            <a href="{{ ($paginator->getCurrentPage() > 1) ? $paginator->getUrl($paginator->getCurrentPage() - 1) : $paginator->getUrl(1) }}">Previous</a>
        </li>
        <li class="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ? 'arrow unavailable' : '' }}">
            <a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}">Next</a>
        </li>
    </ul>
@endif