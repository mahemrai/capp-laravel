@section('content')

    <div class="row" style="padding-top:10px;">
        <div class="small-10 small-centered columns">
            @if($internal)
                <p>{{ $album_info[0]['name'] }}<br>
                {{ $album_info[0]['artist']['name'] }}</p>
                <img src={{ $album_info[0]['image'] }} />
                <p>TrackListing</p>
                <table width="100%">
                    <thead>
                        <th>Track</th>
                        <th>Duration</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($album_info[0]['tracks'] as $track)
                            <tr>
                            <td>{{ $track['name'] }}</td>
                            <td>{{ $track['duration'] }}</td>
                            <td><a class="video-track"
                                 href="" data-reveal-id="videoModal" data-reveal
                                 data="{{ $album_info[0]['artist']['name'].' '.$track['name'] }}">Video</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>{{ $album['album']['name'] }}</p>
                <p>{{ $album['album']['artist'] }}</p>
                <img src={{ $album['album']['image'][2]['#text'] }} />
                <p>TrackListing</p>
                @foreach($album['album']['tracks']['track'] as $track)
                    <p>{{ $track['name'] }}</p>
                @endforeach
            @endif
        </div>
    </div>

     <div id="videoModal" class="reveal-modal medium" data-reveal></div>

    {{ HTML::script('assets/js/albums.js') }}

@stop