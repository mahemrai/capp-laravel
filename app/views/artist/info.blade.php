@section('content')

    <div class="row">
        <div class="small-10 small-centered columns" style="padding-top:10px;">
            <h3>{{ $artist_info[0]['name'] }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <div class="row">
            <!-- Album slider -->
            <div class="small-6 columns">
                <div class="row">
                    <div class="small-6 columns"><h4>Albums</h4></div>
                    <div class="small-6 columns">
                        {{ Form::open(array('url' => '/search/'.$artist_info[0]['name'].'/albums', 'method' => 'POST')) }}
                        {{ Form::hidden('mbid', $artist_info[0]['mbid']) }}
                        {{ Form::submit('Search albums', array('class' => 'button tiny')) }}
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <ul data-orbit>
                        @foreach($artist_info[0]['albums'] as $key=>$album)
                            <li data-orbit-slide="{{ $album['name'] }}">
                                <div>
                                    <h4>{{ $album['name'] }}</h4>
                                    <img src="{{ $album['image'] }}" /><br>
                                    <a class="button tiny" href="/albums/{{ $album['mbid'] }}">View info</a>
                                </div>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <!-- News widget -->
            <div class="small-6 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <h4>News</h4>
                    </div>
                </div>
                <div class="row">
                    <div id="artist-news" class="small-12 columns">
                        @if(empty($news))
                            <p>Information is not available for this artist.</p>
                        @else
                            <ul class="no-bullet">
                            @foreach($news['response']['news'] as $story)
                                <li>
                                    <a target="_blank" href="{{ $story['url'] }}">
                                        <div class="story-container">{{ $story['name'] }}</div>
                                    </a>
                                </li>
                            @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Similar artists section -->
    <div class="row">
        <div class="small-10 small-centered columns">
            <h4>Similar Artists</h4>
            @if(empty($similar_artists))
                <p>Information is not available for this artist.</p>
            @else
                <ul class="small-block-grid-4">
                @foreach($similar_artists['response']['artists'] as $artist)
                    @if(isset($artist['foreign_ids'][0]['foreign_id']))
                        <li>
                            <a href="/artists/{{ trim($artist['foreign_ids'][0]['foreign_id'], 'musicbrainz:artist:') }}">
                                <div class="similar-artist">{{ $artist['name'] }}</div>
                            </a>
                        </li>
                    @endif
                @endforeach
                </ul>
            @endif
        </div>
    </div>
@stop