@section('content')
    <div class="row">
        <div class="small-10 small-centered columns">
            <p>{{ $artist['artist']['name'] }}</p>
            <img src="{{ $artist['artist']['image'][2]['#text'] }}" />
            <p>{{ $artist['artist']['bio']['summary'] }}</p>

            {{ Form::open(array('url' => '/artist/add', 'method' => 'POST')) }}
            {{ Form::hidden('artist', $artist['artist']['name']) }}
            {{ Form::hidden('mbid', $artist['artist']['mbid']) }}
            {{ Form::hidden('image', $artist['artist']['image'][2]['#text']) }}
            {{ Form::submit('Add to my list', array('class' => 'button tiny')) }}
            {{ Form::close() }}
        </div>
    </div>
@stop