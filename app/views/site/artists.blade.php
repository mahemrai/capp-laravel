@section('content')

    <div class="row" style="padding-top:10px;">
        <div class="small-10 small-centered columns">
            <h3>Artists</h3>

            <table width="100%">
                <thead>
                    <th>Name</th>
                    <th></th>
                </thead>
                <tbody>
                @foreach($artists as $artist)
                    <tr>
                        <td>{{ $artist->name }}</td>
                        <td><a class="button tiny" href="/artists/{{ $artist->mbid }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <?php echo $artists->links('layouts.pagination'); ?>
        </div>
    </div>

@stop