@section('content')

    @if(Session::get('message'))
        <div class="row" style="padding-top:10px;">
            <div class="small-10 small-centered columns">
                <div data-alert class="alert-box">
                    <span>{{ Session::get('message') }}</span>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="small-10 small-centered columns" style="padding-top:10px;">
            {{ Form::open(array('url' => '/search/artist', 'method' => 'POST')) }}
            <div class="row">
                <div class="small-8 columns">{{ Form::text('artist', '', array('style' => 'width:620px', 'placeholder' => 'Enter artist')) }}</div>
                <div class="small-3 columns">{{ Form::submit('Search', array('class' => 'button postfix')) }}</div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns" style="min-height:300px;">
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <h3>Recently added artists</h3>
            <hr>
            <ul class="small-block-grid-5">
                @foreach($artists as $artist)
                    <li>
                        {{ $artist['name'] }}<br>
                        <img src="{{ $artist['image'] }}" />
                        <a href="/artists/{{ $artist['mbid'] }}">View</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <h3>Recently added albums</h3>
            <hr>
            <ul class="small-block-grid-5">
                @foreach($albums as $album)
                    <li>
                        {{ $album['name'] }}<br>
                        <img src="{{ $album['image'] }}" />
                        <a href="albums/{{ $album['mbid'] }}">View</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop