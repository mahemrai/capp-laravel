@section('content')

    <div class="row" style="padding-top:10px;">
        <div class="small-10 small-centered columns">
            <h3>Albums</h3>

            <table width="100%">
            <thead>
                <td>Title</td>
                <td>Artist</td>
                <td></td>
            </thead>
            <tbody>
                @foreach($albums as $album)
                <tr>
                    <td>{{ $album->name }}</td>
                    <td>{{ $album->artist['name'] }}</td>
                    <td><a class="button tiny" href="/albums/{{ $album->mbid }}">View</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="small-10 small-centered columns">
            <?php echo $albums->links('layouts.pagination'); ?>
        </div>
    </div>

@stop