<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//SiteController
Route::get('/', 'SiteController@mainPage');
Route::get('/artists', 'SiteController@artistsPage');
Route::get('/albums', 'SiteController@albumsPage');

//SearchController
Route::post('/search/artist', 'SearchController@artistSearch');
Route::post('/search/{artist}/albums', 'SearchController@albumSearch');
Route::post('/search/{artist}/{album}', 'SearchController@albumInfo');

//ArtistController
Route::post('/artist/add', 'ArtistController@addArtist');
Route::get('/artists/{artist}', 'ArtistController@showArtistInfo');

//AlbumController
Route::post('/album/add', 'AlbumController@addAlbum');
Route::get('/albums/{album}', 'AlbumController@albumInfo');

//TrackController
Route::get('/videos/{track}', 'TrackController@showVideo');

?>