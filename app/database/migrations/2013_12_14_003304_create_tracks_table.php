<?php

use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function($table){
            $table->increments('id');
            $table->string('album_id');
            $table->string('name');
            $table->integer('duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tracks');
    }

}