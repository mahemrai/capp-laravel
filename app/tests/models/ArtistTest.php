<?php
use Zizaco\FactoryMuff\Facade\FactoryMuff;

class ArtistTest extends TestCase{
	public function testRelationWithAlbum(){
		$artist = FactoryMuff::create('DbModel\Artist');
		$this->assertEquals($artist->album_id, $artist->album_id);
	}
	
	public function testCreateArtist(){
		$artist = FactoryMuff::create('DbModel\Artist');
		$this->assertEquals($artist->id, $artist->id);
	}
}
?>