<?php
use Zizaco\FactoryMuff\Facade\FactoryMuff;

class AlbumTest extends TestCase{
	public function testRelationWithArtist(){
		$album = FactoryMuff::create('DbModel\Album');
		$this->assertEquals($album->artist_id, $album->artist_id);
	}
}

?>