<?php
use ApiClient\Youtube;

class TrackController extends BaseController{
    protected $layout = 'layouts.master';

    private $youtube;

    public function __construct(){
        $this->youtube = new Youtube();
    }

    public function showVideo($query_string){
        $video = $this->youtube->fetchVideo($query_string);

        return Response::json(array(
            'video' => $video['items']
        ));
    }
}
?>