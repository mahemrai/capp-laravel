<?php
use DbModel\Artist;
use DbModel\Album;

/**
 * ArtistController class
 * @author mahendra
 */

class ArtistController extends BaseController{
    protected $layout = 'layouts.master';

    private $artist;

    //constructor
    public function __construct(){
        $this->artist = new Artist();
    }

    /**
     * Handle request for adding artist to the database.
     */
    public function addArtist(){
        $artist_name = Input::get('artist');
        $mbid = Input::get('mbid');
        $image = Input::get('image');

        //let's first check whether the artist already exists in the database
        //if not then we add the artist details
        if(sizeof($this->artist->fetchArtist('name', $artist_name)) > 0){
            return Redirect::to('/')->with('message', 'Artist already exists.');
        }
        else{
            if($this->artist->createArtist($artist_name, $mbid, $image)){
                return Redirect::to('/')->with('message', 'Artist: '.$artist_name.' successfully added to the list.');
            }
            else return Redirect::to('/')->with('message', 'Artist could not be added.');
        }
    }

    /**
     * Handle request for displaying artist information.
     * @param string $artist_id
     */
    public function showArtistInfo($artist_id){
        if(sizeof($this->artist->fetchArtist('mbid', $artist_id)) <= 0){
            $result = $this->getLastfmObj()->fetchArtistInfo($artist_id);

            $view_data = array(
                    'artist' => json_decode($result, true),
            );

            $this->layout->content = View::make('artist.ext_info', $view_data);
        }
        else{
            $artist_info = $this->artist->fetchArtistInfo($artist_id);
            $news = $this->getEchonestObj()->getArtistNews($artist_id);
            $similar_artists = $this->getEchonestObj()->getSimilarArtists($artist_id);

            $view_data = array(
                    'artist_info' => $artist_info,
                    'news' => $news,
                    'similar_artists' => $similar_artists
            );

            $this->layout->content = View::make('artist.info', $view_data);
        }
    }

    /**
     * Creates a new instance of Album object.
     * @return \DbModel\Album
     */
    protected function getAlbumObj(){
        return new Album();
    }
}
?>