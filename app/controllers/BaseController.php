<?php

use ApiClient\Echonest;
use ApiClient\Lastfm;

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    protected function getLastfmObj(){
        return new Lastfm();
    }

    protected function getEchonestObj(){
        return new Echonest();
    }
}