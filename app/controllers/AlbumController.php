<?php
use DbModel\Artist;
use DbModel\Album;
use DbModel\Track;

/**
 * AlbumController class
 * @author mahendrarai
 */

class AlbumController extends BaseController{
    private $album;

    protected $layout = 'layouts.master';

    //constructor
    public function __construct(){
        $this->album = new Album();
    }

    /**
     * Display album information.
     * @param string $album
     */
    public function albumInfo($album=false){
        if($album){
            $album_info = $this->album->fetchAlbum($album);

            $view_data = array(
                'internal' => true,
                'album_info' => $album_info,
            );

            $this->layout->content = View::make('album.info', $view_data);
        }
    }

    /**
     * Handle request for adding album to the database.
     */
    public function addAlbum(){
        $mbid = Input::get('mbid');
        $name = Input::get('album');
        $image = Input::get('image');
        $artist_id = Input::get('artist_id');

        $result = $this->getLastfmObj()->fetchAlbumInfo($mbid);
        $selected_album = json_decode($result, true);

        if($this->album->albumExists($name)){
            return Redirect::to('/')->with('message', 'Album already exists.');
        }
        else{
            if($this->album->createAlbum($artist_id, $name, $mbid, $image)){
                if($this->getTrackObj()->createTrack($mbid, $selected_album['album']['tracks'])){
                    return Redirect::to('/')->with('message', 'Album: '.$name.' successfully added to the list.');
                }
                else return Redirect::to('/')->with('message', 'Problem occurred while saving the tracks.');
            }
            else return Redirect::to('/')->with('message', 'Problem occurred while saving the album to your list.');
        }
    }

    /**
     * Create a new instance of the Track object.
     */
    protected function getTrackObj(){
        return new Track();
    }
}
?>