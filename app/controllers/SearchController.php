<?php

class SearchController extends BaseController{
    protected $layout = 'layouts.master';

    /**
     *
     */
    public function artistSearch(){
        $artist_name = null;

        if(Input::has('artist')){
            $artist_name = Input::get('artist');
            $result = $this->getLastfmObj()->artistSearch(str_replace(' ', '+', $artist_name));
            $this->layout->content = View::make('search.artists', array('artists' => json_decode($result, true)));
        }
        else{
            return Redirect::to('/')->with('message', 'Please enter name of the artist.');
        }
    }

    /**
     *
     */
    public function albumSearch(){
        $mbid = Input::get('mbid');

        $result = $this->getLastfmObj()->albumSearch($mbid);

        $this->layout->content = View::make('search.albums', array('albums' => json_decode($result, true), 'artist_id' => $mbid));
    }

    /**
     *
     */
    public function albumInfo(){
        $mbid = Input::get('mbid');
        $result = $this->getLastfmObj()->fetchAlbumInfo($mbid);

        $view_data = array(
            'internal' => false,
            'album' => json_decode($result, true)
        );

        $this->layout->content = View::make('album.info', $view_data);
    }
}
?>