<?php
use DbModel\Artist;
use DbModel\Album;

class SiteController extends BaseController{
    private $artist;
    private $album;

    protected $layout = 'layouts.master';

    public function __construct(){
        $this->artist = new Artist();
        $this->album = new Album();
    }

    /**
     * Display main page.
     */
    public function mainPage(){
        $artists = $this->artist->fetchArtists(true);
        $albums = $this->album->fetchAlbums();

        $view_data = array(
                'artists' => $artists,
                'albums' => $albums
        );

        $this->layout->content = View::make('site.main', $view_data);
    }

    /**
     * Display artist listing page.
     */
    public function artistsPage(){
        $artists = $this->artist->fetchArtists();

        $view_data = array(
                'artists' => $artists
        );

        $this->layout->content = View::make('site.artists', $view_data);
    }

    /**
     * Display album listing page.
     */
    public function albumsPage(){
        $albums = $this->album->fetchAllAlbums();

        $view_data = array(
                'albums' => $albums
        );

        $this->layout->content = View::make('site.albums', $view_data);
    }
}
?>