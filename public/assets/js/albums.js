$(document).ready(function(){
    //perform ajax request to get video data and fire up the modal with the embedded
    //youtube video
    $('.video-track').click(function(e){
        e.preventDefault();
        track = $(this).attr('data');

        $.getJSON('/videos/'+track, function(data){
            var embedded_video = '<iframe id=ytplayer type=text/html width=640 height=390 '+
                'src=http://www.youtube.com/embed/'+data.video[0].id.videoId+'?autoplay=1 frameborder=0 />'+
                '<a class="close-reveal-modal">&#215;</a>';

            $('#videoModal').html(embedded_video);
        });
    });
});